Source: ocamlsdl
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>
Build-Depends:
 debhelper (>= 8),
 dh-ocaml (>= 0.9.1),
 ocaml-nox (>= 4),
 libpng-dev,
 libsdl1.2-dev,
 libsdl-mixer1.2-dev,
 libsdl-image1.2-dev,
 libsdl-ttf2.0-dev,
 libsdl-gfx1.2-dev,
 liblablgl-ocaml-dev (>= 1.04),
 ocaml-findlib (>= 1.2.4)
Standards-Version: 3.9.3
Homepage: http://ocamlsdl.sourceforge.net
Vcs-Git: https://salsa.debian.org/ocaml-team/ocamlsdl.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocamlsdl

Package: libsdl-ocaml-dev
Architecture: any
Depends:
 libsdl1.2-dev,
 libsdl-image1.2-dev,
 libsdl-mixer1.2-dev,
 libsdl-ttf2.0-dev,
 libsdl-gfx1.2-dev,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Suggests: ocaml-findlib
Description: OCaml bindings for SDL - development files
 SDL (Simple DirectMedia Layer) is a generic API that provides low
 level access to audio, keyboard, mouse, and display framebuffer
 across multiple platforms.
 OCamlSDL is an OCaml interface to SDL that might be used to write
 multimedia applications in Objective Caml.
 .
 This package contains development files for OCamlSDL application
 writers.

Package: libsdl-ocaml
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Description: OCaml bindings for SDL - runtime files
 SDL (Simple DirectMedia Layer) is a generic API that provides low
 level access to audio, keyboard, mouse, and display framebuffer
 across multiple platforms.
 OCamlSDL is an OCaml interface to the SDL and might be used to write
 multimedia applications in Objective Caml.
 .
 This package contains runtime libraries needed for running dynamic
 bytecode executables.
